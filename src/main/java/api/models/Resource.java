package api.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class Resource {
    private Integer id;

    private String name;

    private Integer year;
    private String color;

    @JsonProperty("pantone_value")
    private String pantoneValue;

}
