package api.steps;

import api.models.*;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;

import static api.steps.SpecHelper.URL;
import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ReqresSteps {

    public static ListUsersResponse getUsersSuccess(){
        return given()
                .spec(SpecHelper.getRequestSpec())
                .when()
                .get("users?page=2")
                .then()
                .spec(SpecHelper.getResponseSpec(200))
                .extract().body().jsonPath().getObject(".", ListUsersResponse.class);
    }

    public static User getUserIdSuccess(){
        return given()
                .param("5")
                .spec(SpecHelper.getRequestSpec())
                .when()
                .get("users/")
                .then()
                .spec(SpecHelper.getResponseSpec(200))
                .extract().body().jsonPath().getObject(".", User.class);
    }

    public static User getUserIdSuccess2(String id){
        return given()
                .spec(SpecHelper.getRequestSpec())
                .when()
                .get(String.format("users/%s", id))
                .then()
                .spec(SpecHelper.getResponseSpec(200))
                .extract().body().jsonPath().getObject(".", User.class);
    }


    public static Response postUserSuccess(CreateUserPayload payload){
        return given()
                .spec(SpecHelper.getRequestSpec())
                .when()
                .body(payload)
                .post("users")
                .then()
                .spec(SpecHelper.getResponseSpec(201))
                .extract()
                .response();
    }

    public static Response postUserRegistrateSuccess(RegisterUserPayload payload){
        return given()
                .spec(SpecHelper.getRequestSpec())
                .when()
                .body(payload)
                .post("login")
                .then()
                .spec(SpecHelper.getResponseSpec(200))
                .extract()
                .response();
    }

    public static Response postUserRegistrateFailed(RegisterUserPayload payload){
        return given()
                .spec(SpecHelper.getRequestSpec())
                .when()
                .body(payload)
                .post("login")
                .then()
                .spec(SpecHelper.getResponseSpec(400))
                .extract()
                .response();
    }


    public static Response putUserSuccess(CreateUserPayload payload){
        return given()
                .spec(SpecHelper.getRequestSpec())
                .when()
                .body(payload)
                .put(URL + "users/2")
                .then()
                .spec(SpecHelper.getResponseSpec(200))
                .extract()
                .response();
    }
    public ListResourceResponse successCheckColorResource() {
        ListResourceResponse response = given()
                .when()
                .contentType(ContentType.JSON)
                .get(URL + "unknown")
                .then()
                .log().all()
                .extract().body().jsonPath().getObject(".", ListResourceResponse.class);
        return response;
    }



    public void getRequest() {
        Response response = given()
                .contentType(ContentType.JSON)
                .when()
                .get(URL + "users/2")
                .then()
                .log().all()
                .extract().response();

        Assertions.assertEquals(200, response.statusCode());
    }




    public void checkEMail(ListUsersResponse response) {
        assertTrue(response.getData().stream().allMatch(x -> x.getEMail().endsWith("@reqres.in")));
    }

    public void checkCreateUser(Response response, CreateUserPayload payload) {
        Assertions.assertEquals(payload.getName(), response.jsonPath().get("name"));
        Assertions.assertEquals(payload.getJob(), response.jsonPath().get("job"));
    }
    public void checkColor(ListResourceResponse response){
        assertTrue(response.getData().stream().allMatch(x -> x.getColor().startsWith("#")));
    }

    public void checkUpdateUser(Response response, CreateUserPayload payload) {
        Assertions.assertEquals(payload.getName(), response.jsonPath().get("name"));
        Assertions.assertEquals(payload.getJob(), response.jsonPath().get("job"));
    }



}
