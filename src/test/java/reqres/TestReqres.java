package reqres;

import api.models.*;
import api.steps.ReqresSteps;
import api.steps.SpecHelper;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static io.restassured.RestAssured.given;

public class TestReqres {

    private final static String URL = "https://reqres.in/api/";
    private final ReqresSteps reqresSteps = new ReqresSteps();

    //проверка соответствия номера id
    @Test
    public void successCheckAvatar(){
        List<User> users = given()
                .when()
                .contentType(ContentType.JSON)
                .get( URL + "users?page=2")
                .then()
                .log().all()
                .extract().body().jsonPath().getList("data", User.class);

        users.stream().forEach(x -> Assertions.assertTrue(x.getAvatar().contains(x.getId().toString())));
    }



    //получение информации о пользователе с id=2, метод GET
    @Test
    public void successGetuserId() {
        Response response = given()
                .contentType(ContentType.JSON)
                .when()
                .get( URL + "users/2")
                .then()
                .log().all()
                .extract().response();

        Assertions.assertEquals(200, response.statusCode());
    }


    //успешное удаление пользователя, метод DELETE
    @Test
    public void successDeleteUser() {
        Response response = given()
                .contentType(ContentType.JSON)
                .when()
                .delete( URL + "users/2")
                .then()
                .log().all()
                .extract().response();

        Assertions.assertEquals(204, response.statusCode());
    }




    //получение информации о пользователе с некорректным id, метод GET
    // ожидаемый статус код 404
    @Test
    public void unsuccessGetuserId() {
        Response response = given()
                .contentType(ContentType.JSON)
                .when()
                .get( URL + "users/-6")
                .then()
                .log().all()
                .extract().response();

        Assertions.assertEquals(404, response.statusCode());
    }


    //проверка, что все имейлы заканчиваются на @reqres.in
    @Test
    public void successCheckEmail1() {
        ListUsersResponse response = given()
                .when()
                .contentType(ContentType.JSON)
                .get(URL + "users?page=2")
                .then()
                .log().all()
                .extract().body().jsonPath().getObject(".", ListUsersResponse.class);

        Assertions.assertTrue(response.getData().stream().allMatch(x -> x.getEMail().endsWith("@reqres.in")));
    }

    //аналогично логике теста выше
    @Test
    public void successCheckEmail2() {
        ListUsersResponse response = reqresSteps.getUsersSuccess();
        reqresSteps.checkEMail(response);
    }


    //создание нового пользователя POST login
    @Test
    public void successCreateUser() {
        CreateUserPayload payload = new CreateUserPayload("Alina", "qa");
        Response response = reqresSteps.postUserSuccess(payload);
        reqresSteps.checkCreateUser(response, payload);
    }

    // негативный тест-кейс, создание нового пользователя без указания работы, баг? POST login
    @Test
    public void failedCreateUser() {
        CreateUserPayload payload = new CreateUserPayload("Alina", "");
        Response response = reqresSteps.postUserSuccess(payload);
        reqresSteps.checkCreateUser(response, payload);
    }


    //успешная регистрация пользователя POST register
    @Test
    public void successRegistrateUser() {
        RegisterUserPayload payload = new RegisterUserPayload("eve.holt@reqres.in", "pistol");
        Response response = reqresSteps.postUserRegistrateSuccess(payload);
    }


    //попытка регистрации с некорректным имейлом, ожидаем статус код 400, POST register
    @Test
    public void failedRegistrateUser() {
        RegisterUserPayload payload = new RegisterUserPayload("eve", "pistol");
        Response response = reqresSteps.postUserRegistrateFailed(payload);
    }


    //проверка на то, что каждый цвет начинается с символа #, метод GET
    @Test
    public void checkColorResource(){
        ListResourceResponse response = reqresSteps.successCheckColorResource();
        reqresSteps.checkColor(response);
    }


    //проверка обновления данных пользователя, метод PUT
    @Test
    public void updateInfoUser(){
        CreateUserPayload payload = new CreateUserPayload("morpheus","zion resident");
        Response response = reqresSteps.putUserSuccess(payload);
        reqresSteps.checkUpdateUser(response, payload);
    }

}
